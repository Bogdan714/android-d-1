package com.example.lenovo.listhw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.phone)
    TextView phone;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        if (getIntent().hasExtra(MainActivity.KEY_VALUE)) {
            user = getIntent().getParcelableExtra(MainActivity.KEY_VALUE);
        }
        icon.setImageResource(user.getIcon());
        name.setText(user.getName());
        email.setText("email: " + user.getEmail());
        phone.setText("phone: " + user.getPhone());
        address.setText("address: " + user.getAddress());
    }
}
