package com.example.lenovo.listhw;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wenceslaus on 19.05.18.
 */

public class User implements Parcelable {

    private String name;
    private String email;
    private String address;
    private String phone;
    private int icon;

    public User(String name, String email, String address, String phone, int icon) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public int getIcon() {
        return icon;
    }

    protected User(Parcel in) {
        name = in.readString();
        email = in.readString();
        address = in.readString();
        phone = in.readString();
        icon = in.readInt();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeInt(icon);
    }
}
