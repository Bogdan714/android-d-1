package com.example.lenovo.listhw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    MyAdapter myAdapter;
    public static final String KEY_VALUE = "key";

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.address)
    EditText address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        myAdapter = new MyAdapter(this, generateUsers());
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(myAdapter);
    }

    @OnItemClick(R.id.list)
    public void onItemClick(int position) {
        Toast.makeText(MainActivity.this, myAdapter.getItem(position).getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(KEY_VALUE, myAdapter.getItem(position));
        startActivity(intent);
    }

    @OnClick(R.id.button_add)
    public void onClick() {
        String[] params = {name.getText().toString(),
                email.getText().toString(),
                address.getText().toString(),
                phone.getText().toString()};
        boolean correctInput = true;
        for (int i = 0; i < params.length; i++) {
            if (params[i].equalsIgnoreCase("")) {
                correctInput = false;
            }
        }
        if (correctInput) {
            myAdapter.addItem(new User(params[0],
                    params[1],
                    params[2],
                    params[3],
                    R.mipmap.ic_launcher));
            Toast.makeText(MainActivity.this, "added", Toast.LENGTH_SHORT).show();
            name.setText("");
            email.setText("");
            address.setText("");
            phone.setText("");
        }
    }

    public List<User> generateUsers() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(new User("Vasya" + i,
                    "Vasya" + i + "@gmail.com",
                    i + "-street, " + i,
                    "+38 (050) 123 456" + (i % 10),
                    R.mipmap.ic_launcher));
        }
        return list;
    }

}